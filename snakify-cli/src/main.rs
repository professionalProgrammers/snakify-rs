extern crate clap;
extern crate snakify;

use clap::{
    App,
    Arg,
    SubCommand,
};
use snakify::{
    Client,
    TeacherStatus,
};

fn main() {
    let matches = App::new("snakify-cli")
        .subcommand(
            SubCommand::with_name("model")
                .arg(Arg::with_name("name").required(true).index(1))
                .arg(
                    Arg::with_name("username")
                        .short("u")
                        .long("username")
                        .required(true)
                        .takes_value(true),
                )
                .arg(
                    Arg::with_name("password")
                        .short("p")
                        .long("password")
                        .required(true)
                        .takes_value(true),
                ),
        )
        .subcommand(
            SubCommand::with_name("list")
                .arg(
                    Arg::with_name("username")
                        .short("u")
                        .long("username")
                        .takes_value(true),
                )
                .arg(
                    Arg::with_name("password")
                        .short("p")
                        .long("password")
                        .takes_value(true),
                ),
        )
        .get_matches();

    match matches.subcommand() {
        ("model", Some(matches)) => {
            let mut client = Client::new();
            client
                .login(
                    matches.value_of("username").unwrap(),
                    matches.value_of("password").unwrap(),
                    TeacherStatus::Student,
                )
                .unwrap();

            let model = client
                .get_model_solution(matches.value_of("name").unwrap())
                .unwrap();
            println!("{}", model.code);
        }
        ("list", Some(matches)) => {
            let mut client = Client::new();
            let ops = (matches.value_of("username"), matches.value_of("password"));
            if ops.0.is_some() && ops.1.is_none() {
                println!("Missing Password");
                return;
            }

            if ops.1.is_some() && ops.0.is_none() {
                println!("Missing Username");
                return;
            }

            if ops.1.is_some() && ops.0.is_some() {
                client
                    .login(
                        matches.value_of("username").unwrap(),
                        matches.value_of("password").unwrap(),
                        TeacherStatus::Student,
                    )
                    .unwrap();
            }

            let profile = client.get_profile().unwrap();
            for el in profile.lessons.iter() {
                println!("-{}", el.title);
                for el in el.problems.iter() {
                    println!("\t+{}", el.title);
                }
            }
        }
        _ => {
            println!("Unknown Command");
        }
    }
}
