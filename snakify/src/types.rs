use serde::Deserialize;

#[derive(Debug, Deserialize)]
pub struct Login {
    pub status: String,
}

#[derive(Debug, Deserialize)]
pub struct CheckAccount {
    exists: bool,
    name: String,
}

#[derive(Debug, Deserialize)]
pub struct GetScoreboard {
    #[serde(rename = "isFrontend")]
    is_frontend: Option<bool>,
    #[serde(rename = "isTeacher")]
    is_teacher: bool,
    #[serde(rename = "joinClassLink")]
    join_class_link: Option<String>,
    lessons: Vec<Lesson>,
    #[serde(rename = "otherTypeUrl")]
    other_type_url: String,
    squads: Vec<serde_json::Value>,
    students: Vec<serde_json::Value>,
}

#[derive(Debug, Deserialize)]
pub struct Lesson {
    #[serde(rename = "maxScore")]
    max_score: u32,
    pub title: String,
    urlname: String,
}

#[derive(Debug, Deserialize)]
pub struct ModelSolution {
    pub code: String,
}
