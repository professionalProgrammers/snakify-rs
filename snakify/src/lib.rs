pub mod client;
pub mod types;

use crate::types::{
    CheckAccount,
    GetScoreboard,
    Login,
    ModelSolution,
};
use reqwest::{
    header::{
        COOKIE,
        SET_COOKIE,
    },
    Client as ReqwestClient,
    Method,
    Request,
    Response,
    Url,
};
use select::{
    document::Document,
    predicate::{
        And,
        Class,
        Name,
        Text,
    },
};
use serde_json::json;

#[derive(Debug)]
pub enum SnakifyError {
    BadUrl,
    Network,
    InvalidCookie,
    InvalidJson,
    InvalidSession,
    InvalidLogin,
    InvalidBody,
}

pub type SnakifyResult<T> = Result<T, SnakifyError>;

pub struct Client {
    client: ReqwestClient,
    sessionid: Option<String>,
}

impl Client {
    pub fn new() -> Self {
        Client {
            client: ReqwestClient::new(),
            sessionid: None,
        }
    }

    pub fn send_request(&mut self, req: Request) -> SnakifyResult<Response> {
        let mut response = self
            .client
            .execute(req)
            .map_err(|_| SnakifyError::Network)?;
        if response.status().is_success() {
            if let Some(cookie) = response
                .headers()
                .get_all(SET_COOKIE)
                .iter()
                .find(|el| el.as_bytes().starts_with(b"sessionid"))
            {
                let mut iter = cookie.as_bytes().iter(); //sessionid=6z197r6csvqt4fid3wix9t8sqs89h2zx;
                let start = iter
                    .position(|&el| el == b'=')
                    .ok_or_else(|| SnakifyError::InvalidCookie)?
                    + 1;
                let end = iter
                    .position(|&el| el == b';')
                    .ok_or_else(|| SnakifyError::InvalidCookie)?
                    + start;
                self.sessionid = Some(
                    String::from_utf8(cookie.as_bytes()[start..end].to_vec())
                        .map_err(|_| SnakifyError::InvalidCookie)?,
                );
            }
            Ok(response)
        } else {
            dbg!(&response);
            let body = response.text().unwrap();
            dbg!(body);
            Err(SnakifyError::Network)
        }
    }

    pub fn account_exists(&mut self, email: &str) -> SnakifyResult<CheckAccount> {
        let mut url: Url = "https://snakify.org/check_login/"
            .parse()
            .map_err(|_| SnakifyError::BadUrl)?;
        url.query_pairs_mut().append_pair("email", email);

        let req = self
            .client
            .request(Method::GET, url)
            .build()
            .map_err(|_| SnakifyError::BadUrl)?;

        self.send_request(req)?
            .json()
            .map_err(|_| SnakifyError::InvalidJson)
    }

    pub fn login(
        &mut self,
        email: &str,
        password: &str,
        status: TeacherStatus,
    ) -> SnakifyResult<Login> {
        let login_url: Url = "https://snakify.org/login/"
            .parse()
            .map_err(|_| SnakifyError::BadUrl)?;

        let form = &[
            ("email", email),
            ("first_name", ""),
            ("last_name", ""),
            ("password", password),
            ("teacher_status", status.to_str()),
            ("teacher_email", ""),
            ("referrer_id", ""),
        ];

        let req = self
            .client
            .request(Method::POST, login_url)
            .form(&form)
            .build()
            .map_err(|_| SnakifyError::BadUrl)?;

        let res: Login = self
            .send_request(req)?
            .json()
            .map_err(|_| SnakifyError::InvalidJson)?;
        if res.status != "success" {
            Err(SnakifyError::InvalidLogin)
        } else {
            Ok(res)
        }
    }

    pub fn get_scoreboard(&mut self) -> SnakifyResult<GetScoreboard> {
        let session = self
            .sessionid
            .as_ref()
            .ok_or_else(|| SnakifyError::InvalidSession)?;
        let url: Url = "https://snakify.org/api/v2/teacher/getScoreboard"
            .parse()
            .map_err(|_| SnakifyError::BadUrl)?;

        let req = self
            .client
            .request(Method::POST, url)
            .header(COOKIE, format!("sessionid={}", session).as_str())
            .json(&json!({"isFrontend": false}))
            .build()
            .map_err(|_| SnakifyError::BadUrl)?;

        self.send_request(req)?
            .json()
            .map_err(|_| SnakifyError::InvalidJson)
    }

    pub fn get_model_solution(&mut self, problem: &str) -> SnakifyResult<ModelSolution> {
        let session = self
            .sessionid
            .as_ref()
            .ok_or_else(|| SnakifyError::InvalidSession)?;

        let url: Url = "https://snakify.org/api/v2/problem/modelSolution"
            .parse()
            .map_err(|_| SnakifyError::BadUrl)?;

        let req = self
            .client
            .request(Method::POST, url)
            .header(COOKIE, format!("sessionid={}", session).as_str())
            .json(&json!({"problemUrlname": problem, "language": "python"})) //TODO: Other Languages
            .build()
            .map_err(|_| SnakifyError::BadUrl)?;

        self.send_request(req)?
            .json()
            .map_err(|_| SnakifyError::InvalidJson)
    }

    pub fn get_profile(&mut self) -> SnakifyResult<Profile> {
        let session = self
            .sessionid
            .as_ref()
            .map(|el| el.as_str())
            .unwrap_or_else(|| "");

        let url: Url = "https://snakify.org/profile/"
            .parse()
            .map_err(|_| SnakifyError::BadUrl)?;

        let req = self
            .client
            .request(Method::GET, url)
            .header(COOKIE, format!("sessionid={}", session).as_str())
            .build()
            .map_err(|_| SnakifyError::BadUrl)?;

        let body = self
            .send_request(req)?
            .text()
            .map_err(|_| SnakifyError::InvalidBody)?;
        Profile::from_body(&body)
    }
}

//Only useful if your making accounts?
pub enum TeacherStatus {
    Teacher,
    Student,
    Neither,
}

impl TeacherStatus {
    pub fn to_str(&self) -> &str {
        match self {
            TeacherStatus::Teacher => "is-teacher",
            TeacherStatus::Student => "has-teacher",
            TeacherStatus::Neither => "neither",
        }
    }
}

//Api Responses

#[derive(Debug)]
pub struct Profile {
	pub lessons: Vec<ProfileLesson>
}

impl Profile {
    fn from_body(body: &str) -> SnakifyResult<Self> {
        let lessons = Document::from(body)
            .find(And(Name("div"), Class("user-progress")))
            .last()
            .ok_or(SnakifyError::InvalidBody)?
            .find(Name("a"))
            .filter_map(|el| {
                let href = el.attr("href")?.to_string();
                let title = el.find(Text).last()?.as_text()?.to_string();
                let is_section = !el.is(Class("btn"));
                let state = if el.is(Class("problem-status__ok")) {
                    Some(ProfileProblemState::Ok)
                } else if el.is(Class("problem-status__error")) {
                    Some(ProfileProblemState::Err)
                } else if el.is(Class("problem-status__")) {
                    Some(ProfileProblemState::None)
                } else {
                    None
                };

                Some((href, title, is_section, state))
            })
            .fold(
                Vec::new(),
                |mut state, (href, title, is_section, status)| {
                    if is_section {
                        state.push(ProfileLesson {
                            title,
                            href,
                            problems: Vec::new(),
                        });
                    } else if let Some(status) = status {
                        state.last_mut().unwrap().problems.push(ProfileProblem {
                            title,
                            href,
                            status,
                        });
                    }
                    state
                },
            );

        Ok(Profile {
			lessons
		})
    }
}

#[derive(Debug, Default)]
pub struct ProfileLesson {
    pub title: String,
    href: String,
    pub problems: Vec<ProfileProblem>,
}

#[derive(Debug)]
pub struct ProfileProblem {
    pub title: String,
    href: String,
    status: ProfileProblemState,
}

#[derive(Debug)]
pub enum ProfileProblemState {
    Ok,
    Err,
    None,
}
