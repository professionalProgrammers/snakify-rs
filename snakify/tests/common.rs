use snakify::{
    Client,
    TeacherStatus,
};

fn get_student_pass() -> String {
    String::from("wildct!23")
}

#[test]
fn common() {
    let mut client = Client::new();
    let email = "nathaniel.daniel@rocklinusd.org";
    client
        .login(email, &get_student_pass(), TeacherStatus::Student)
        .unwrap();
    dbg!(client.get_profile().unwrap());
}

#[test]
fn account_exists() {
    let mut client = Client::new();
    let email = "nathaniel.daniel@rocklinusd.org";
    let res = client.account_exists(email).unwrap();
    dbg!(res);
}

#[test]
fn login_student() {
    let mut client = Client::new();
    let email = "nathaniel.daniel@rocklinusd.org";
    let password = "wildct!23";
    let _res = client
        .login(email, password, TeacherStatus::Student)
        .unwrap();
}

#[test]
fn get_scoreboard() {
    let mut client = Client::new();
    let email = "paul.werner@rocklinusd.org";
    let password = "j7riotwerner";
    client.login(email, password, TeacherStatus::Teacher);
    dbg!(client.get_scoreboard().unwrap());
}

#[test]
fn get_model_solution() {
    let mut client = Client::new();
    let email = "nathaniel.daniel@rocklinusd.org";
    let password = "wildct!23";
    let res = client
        .login(email, password, TeacherStatus::Student)
        .unwrap();
    let solution = client.get_model_solution("is_three_digit").unwrap();
    println!("{}", solution.code);
}
